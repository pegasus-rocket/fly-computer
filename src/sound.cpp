#include <sound.h>

unsigned int currentNote = 0;
unsigned long noteDuration;
unsigned long noteStart;

int readyNotes[] = {3000, -1};
int readyDurations[] = {100, 1000};

int flyingNotes[] = {500, 1000, 1500, 2000, 2500, -1};
int flyingDurations[] = {50, 50, 75, 100, 100, 500};

int errorNotes[] = {4000, -1};
int errorDurations[] = {100, 50};

int selectedSound = NO_SOUND;

void playSound() {
#ifdef DEVELOPMENT_ENABLED
    return;
#endif
    if (millis() - noteStart < noteDuration) {
        return;
    }

    if (selectedSound == NO_SOUND) {
        return noTone(BUZZER_PIN);
    }

    auto *sound = new Sound;

    const int *const sounds[NO_SOUND] = {errorNotes, readyNotes, flyingNotes};
    const int *const durations[NO_SOUND] = {
        errorDurations, readyDurations,
        flyingDurations
    };
    const size_t sizes[3] = {
        sizeof(errorNotes) / sizeof(int),
        sizeof(readyNotes) / sizeof(int),
        sizeof(flyingNotes) / sizeof(int)
    };
    sound->notes = new int[sizes[selectedSound]];
    sound->durations = new int[sizes[selectedSound]];
    sound->numberOfNotes = sizes[selectedSound];

    // Corrected memcpy
    memcpy(sound->notes, sounds[selectedSound],
           sizes[selectedSound] * sizeof(int));
    memcpy(sound->durations, durations[selectedSound],
           sizes[selectedSound] * sizeof(int));

    // Stop the current note
    noTone(BUZZER_PIN);

    // Move to the next note
    currentNote++;

    // If all notes have been played, reset to the beginning
    if (currentNote >= sound->numberOfNotes) {
        currentNote = 0;
    }

    // Play the next note
    if (sound->notes[currentNote] == -1) {
        noTone(BUZZER_PIN);
    } else {
        tone(BUZZER_PIN, sound->notes[currentNote]);
    }
    noteDuration = sound->durations[currentNote];
    noteStart = millis();

    delete[] sound->notes;
    delete[] sound->durations;
    delete sound;
}

void changeSound(int sound) {
    if (sound == ERROR_SOUND || sound == READY_SOUND || sound == FLYING_SOUND ||
        sound == NO_SOUND) {
        selectedSound = sound;
    }
}


void playTone(const unsigned int frequency, const unsigned long duration = 0) {
    tone(BUZZER_PIN, frequency, duration);
}


void stopTone() {
    noTone(BUZZER_PIN);
}