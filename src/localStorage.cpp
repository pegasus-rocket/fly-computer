#include "localStorage.h"

ls::LocalStorage localStorage;

void ls::LocalStorage::printDir(const char *dirname, uint8_t levels) {
#if ENABLE_LOCAL_STORAGE
    Sl.debug("Listing directory: %s\r\n", dirname);

    File root = fs.open(dirname);
    if (!root) {
        Sl.error("LocalStorage - failed to open directory\n");
        return;
    }
    if (!root.isDirectory()) {
        Sl.error("LocalStorage - not a directory\n");
        return;
    }

    File file = root.openNextFile();
    while (file) {
        if (file.isDirectory()) {
            Serial.print("  DIR : ");
            Serial.println(file.name());
            if (levels) {
                printDir(file.path(), levels - 1);
            }
        } else {
            Serial.print("  FILE: ");
            Serial.print(file.name());
            Serial.print("\tSIZE: ");
            Serial.println(file.size());
        }
        file = root.openNextFile();
    }
#endif
}

void ls::LocalStorage::readFile(const char *path) const {
#if ENABLE_LOCAL_STORAGE
    Sl.info("LocalStorage - Reading file\n");

    File file = fs.open(path);
    if (!file || file.isDirectory()) {
        Sl.error("LocalStorage - failed to open file for reading\n");
        return;
    }

    Sl.info("LocalStorage - Reading file");
    while (file.available()) {
        Serial.write(file.read());
    }
    file.close();
#endif
}


bool ls::LocalStorage::writeFile(const char *path, const char *message) const {
#if ENABLE_LOCAL_STORAGE
    Sl.debug("LocalStorage - Writing file: %s\n", path);
    Serial.println(path);

    File file = fs.open(path, FILE_WRITE);
    if (!file) {
        Sl.error("LocalStorage - failed to open file for writing\n");
        return false;
    }
    if (file.print(message)) {
        Sl.info("LocalStorage - successfully written\n");
    } else {
        Sl.error("LocalStorage - failed to write to file\n");
        file.close();
        return false;
    }
    file.close();
    return true;
#else
    Sl.info("LocalStorage is not enabled");
#endif
}


bool ls::LocalStorage::appendFile(const char *path, const String &message) const {
#if ENABLE_LOCAL_STORAGE
    Sl.debug("LocalStorage - Writing file: %s\n", path);

    File file = fs.open(path, FILE_APPEND);
    if (!file) {
        Sl.error("LocalStorage - failed to open file for appending\n");
        return false;
    }
    if (file.print(message)) {
        Sl.info("LocalStorage - successfully written\n");
    } else {
        Sl.error("LocalStorage - failed to write to file\n");
        file.close();
        return false;
    }
    file.close();
    return true;
#else
    return false;
#endif
}

size_t ls::LocalStorage::fileSize(const char *path) const {
#if ENABLE_LOCAL_STORAGE
    const File file = fs.open(path);
    return (file) ? file.size() : 0;
#endif
}


void ls::LocalStorage::renameFile(const char *path1, const char *path2) const {
#if ENABLE_LOCAL_STORAGE
    Sl.info("LocalStorage - Renaming file %s to %s\r\n", path1, path2);
    if (fs.rename(path1, path2)) {
        Sl.debug("LocalStorage - successfully renamed %s to %s", path1, path2);
    } else {
        Sl.error("LocalStorage - failed to rename file: %s\n", path1);
    }
#endif
}

bool ls::LocalStorage::deleteFile(const char *path) const {
#if ENABLE_LOCAL_STORAGE
    return fs.remove(path);
#else
    return false;
#endif
}


void ls::LocalStorage::getFilesPath(const char *dirname, uint8_t levels, list<string> *paths) {
#if ENABLE_LOCAL_STORAGE
    File root = fs.open(dirname);
    if (!root || !root.isDirectory()) return;

    File file = root.openNextFile();
    while (file) {
        if (file.isDirectory()) {
            if (levels) {
                getFilesPath(file.path(), levels - 1, paths);
            }
        } else {
            paths->emplace_back(file.path());
        }
        file = root.openNextFile();
    }
#endif
}

bool ls::LocalStorage::exists(const char *path) const {
#if ENABLE_LOCAL_STORAGE
    return fs.exists(path);
#else
    return false;
#endif
}

string ls::LocalStorage::getFileExtension(const string &path) {
    size_t dotPosition = path.find_last_of('.');

    // If no '.' was found, or it's the last character (no extension), return an empty string
    if (dotPosition == std::string::npos || dotPosition == path.length() - 1) {
        return "";
    }

    return path.substr(dotPosition + 1);
}
