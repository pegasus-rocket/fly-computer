#include "radio.h"
#include "fsLog.h"
#include "Rocket.h"

volatile bool Radio::operationDone;

int16_t Radio::begin() {
    int16_t status = sx1262.beginFSK(
        RADIO_FREQ,
        RADIO_BR,
        RADIO_FREQ_DEV,
        RADIO_RXBW,
        RADIO_POWER,
        RADIO_PREAMBLE_LENGTH);

    if (status != RADIOLIB_ERR_NONE) {
        systemLog.append(ERROR, "Radio initialization failed\nStatus: %d", status);
        return status;
    }

    uint8_t sync_word[] = {0xAA, 0x2D};
    status = sx1262.setSyncWord(sync_word, 2);

    if (status != RADIOLIB_ERR_NONE) {
        systemLog.append(ERROR, "Radio (sx1262) set sync word failed.\nStatus: %d", status);
        return status;
    }

    operationDone = false;
    transmitFlag = false;
    sx1262.setDio1Action(setOperationDone);

    status = sx1262.setEncoding(RADIOLIB_ENCODING_WHITENING);

    if (status != RADIOLIB_ERR_NONE) {
        systemLog.append(ERROR, "Radio set whitening encoding failed\nStatus: %d", status);
        return status;
    }

    status = sx1262.startReceive();
    return status;
}

void Radio::update() {
    if (operationDone) {
        operationDone = false;
        if (transmitFlag) {
            // the previous operation was a transmission, listen for response

            transmissionState = sx1262.finishTransmit();
            if (transmissionState != RADIOLIB_ERR_NONE) {
                Sl.debug("Radio transmission failed\nTransmission status: %d\n", transmissionState);
            }
            transmitFlag = false;
            sx1262.startReceive();
        } else {
            const size_t payloadSize = sx1262.getPacketLength();
            if (payloadSize > 0 && payloadSize < RADIO_MAX_PAYLOAD_SIZE) {
                uint8_t buffer[sizeof(RadioHeader) + (RADIO_MAX_PAYLOAD_SIZE - sizeof(RadioHeader))];
                const int state = sx1262.readData(buffer, sizeof(buffer));
                if (state == RADIOLIB_ERR_NONE) {
                    const auto *header = reinterpret_cast<RadioHeader *>(buffer);

                    if (eventListenerStore.count(header->type)) {
                        const auto callbacks = eventListenerStore.find(header->type); // Functions callbacks

                        uint16_t message[header->length];
                        memcpy(&message, header->data, header->length);

                        for (const auto &callback: callbacks->second) {
                            callback(message);
                        }
                    } else {
                        Sl.debug(
                            "*** WARNING *** Radio unknown received message type -> Type: %d\n", header->type);
                    }
                } else if (state == RADIOLIB_ERR_CRC_MISMATCH) {
                    // packet was received, but is malformed
                    Sl.error("Radio CRC error!\n");
                } else {
                    // some other error occurred
                    Sl.error("Radio failed, code! state: %d\n", state);
                }
            } else {
                Sl.warning("Radio received unexpected data length! Payload size: %d\n", payloadSize);
            }
        }
    }

    if (!messageQueue.empty() && !transmitFlag) {
        const unique_ptr<RadioMessage> message = move(messageQueue.front());
        if (message) {
            uint8_t buffer[sizeof(RadioHeader) + message->len];
            auto *header = reinterpret_cast<RadioHeader *>(buffer);
            header->type = message->messageType; // Rocket message type
            header->length = message->len;

            memcpy(header->data, message->message, message->len);

            sx1262.startTransmit(buffer, sizeof(buffer));
        }

        messageQueue.pop_front();
        transmitFlag = true;
    }
}


void Radio::sendMessage(RadioMessage *message) {
    if (messageQueue.size() > 10) {
        Sl.warning("Radio message queue is too big.\n");
        for (int i = 0; i < messageQueue.size() - 5; i++) {
            messageQueue.pop_front();
        }
    }
    messageQueue.emplace_back(message);
}
