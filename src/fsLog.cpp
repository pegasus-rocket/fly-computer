#include "fsLog.h"
#include "utils.h"
#include <settings.h>
#include <localStorage.h>

logging::Logging systemLog(SYSTEM_LOG_PATH);
logging::Logging flyData(FLY_DATA_LOG_PATH);

void logging::Logging::append(const LogLevel logLevel, const char *format, ...) const {
        va_list args;
        va_start(args, format);

        // Create a buffer to hold the formatted string
        char formattedString[1024];
        vsnprintf(formattedString, sizeof(formattedString), format, args);

        va_end(args);

        // Create the log line
        String line;
        line +=
            String(getHours()) + ":" +
            String(getMinutes()) + ":" +
            String(getSecond()) + ":" +
            String(getMillis()) +
            "\t => " + formattedString + "\n";

        // Log the line
        Sl.log(logLevel, line.c_str());

        // Append to the buffer
        buffer += line;
    }

void logging::Logging::taskWrapper(void *pvParameters) {
    const auto *instance = static_cast<Logging *>(pvParameters);
    if (instance) {
        for (;;) {
            instance->flush();
            vTaskDelay(10000 / portTICK_PERIOD_MS);
        }
    }
}

void logging::Logging::flush() const {
    if (buffer.length() > 50000) {
        buffer = "";
        return;
    };
    if (buffer.isEmpty()) return;

    if (!localStorage.appendFile(log_path, buffer)) {
        Sl.error("Failed to append log file into Local Storage\n");
        return;
    }
    buffer = "";
    Sl.debug("Logging::Logging::flush()\n");
}

void logging::Logging::printLog() const {
    localStorage.readFile(log_path);
    Serial.println();
}

bool logging::Logging::deleteLogs() const {
    return localStorage.deleteFile(log_path);
}
