#include "matrix.h"
#include "Arduino.h"

float Mat3_Det(const Mat3 &mat) {
    return mat.data[0][0] * (mat.data[1][1] * mat.data[2][2] - mat.data[2][1] * mat.data[1][2]) -
           mat.data[0][1] * (mat.data[1][0] * mat.data[2][2] - mat.data[2][0] * mat.data[1][2]) +
           mat.data[0][2] * (mat.data[1][0] * mat.data[2][1] - mat.data[2][0] * mat.data[1][1]);
}

Mat3 Mat3_Sale(Mat3 matrix, float s) {
    for (auto &i: matrix.data) {
        for (float &j: i) {
            j *= s;
        }
    }
    return matrix;
}

Vec3 *Mat3_MulMV(const Mat3 &mat, const Vec3* v) {
    Vec3 *res = Vec3_Set(0, 0, 0);
    for (int i = 0; i < 3; i++) {
        for (int j = 0; j < 3; j++) {
            res->data[i] += mat.data[i][j] * v->data[j];
        }
    }

    return res;
}

Mat3 Mat3_MulMM(const Mat3 &mat1, const Mat3 &mat2) {
    Mat3 res = {0};
    for (int i = 0; i < 3; ++i) {
        for (int j = 0; j < 3; ++j) {
            for (int k = 0; k < 3; ++k) {
                res.data[i][j] += mat1.data[i][k] * mat2.data[k][j];
            }
        }
    }
    return res;
}

void Mat3_printMatrix(Mat3 matrix) {
    for (auto &i: matrix.data) {
        for (float j: i) {
            Serial.print(j, 2);
            Serial.print(" ");
        }
        Serial.println("");
    }
    Serial.println("");
}

Mat3 Mat3_rotationX(float angle) {
    Mat3 rX;

    rX.data[0][0] = 1;
    rX.data[0][1] = 0;
    rX.data[0][2] = 0;

    rX.data[1][0] = 0;
    rX.data[1][1] = cos(angle);
    rX.data[1][2] = -sin(angle);

    rX.data[2][0] = 0;
    rX.data[2][1] = sin(angle);
    rX.data[2][2] = cos(angle);

    return rX;
}

Mat3 Mat3_rotationY(float angle) {
    Mat3 rY;

    rY.data[0][0] = cos(angle);
    rY.data[0][1] = 0;
    rY.data[0][2] = sin(angle);

    rY.data[1][0] = 0;
    rY.data[1][1] = 1;
    rY.data[1][2] = 0;

    rY.data[2][0] = -sin(angle);
    rY.data[2][1] = 0;
    rY.data[2][2] = cos(angle);

    return rY;
}

Mat3 Mat3_rotationZ(float angle) {
    Mat3 rZ;

    rZ.data[0][0] = cos(angle);
    rZ.data[0][1] = -sin(angle);
    rZ.data[0][2] = 0;

    rZ.data[1][0] = sin(angle);
    rZ.data[1][1] = cos(angle);
    rZ.data[1][2] = 0;

    rZ.data[2][0] = 0;
    rZ.data[2][1] = 0;
    rZ.data[2][2] = 1;

    return rZ;
}


Mat4 Mat4_eulerToQuat(const Vec3 *v) {
    Vec4 *vector = Vec4_Set(v->x, v->y, v->z, 0.f);
    return Mat4_setQua(vector);
}

Mat4 Mat4_setQua(const Vec4 *vector) {
    Mat4 res;

    res.data[0][0] = vector->w;
    res.data[0][1] = -vector->x;
    res.data[0][2] = -vector->y;
    res.data[0][3] = -vector->z;

    res.data[1][0] = vector->x;
    res.data[1][1] = vector->w;
    res.data[1][2] = -vector->z;
    res.data[1][3] = vector->y;

    res.data[2][0] = vector->y;
    res.data[2][1] = vector->z;
    res.data[2][2] = vector->w;
    res.data[2][3] = -vector->x;

    res.data[3][0] = vector->z;
    res.data[3][1] = -vector->y;
    res.data[3][2] = vector->x;
    res.data[3][3] = vector->w;

    return res;
}

Vec4 *Mat4_MulMV(const Mat4 &mat, const Vec4 *v) {
    Vec4 *res = Vec4_Set(0, 0, 0, 0);
    for (int i = 0; i < 4; i++) {
        for (int j = 0; j < 4; j++) {
            res->data[i] += mat.data[i][j] * v->data[j];
        }
    }

    return res;
}

float Mat4_Cofactor(const Mat4 &mat, const int i, const int j) {
    Mat3 cofactor;

    int rowActivated = 0, colActivated = 0;

    for (int x = 0; x < 4; ++x) {
        if (x == i) {
            rowActivated = 1;
            continue;
        }

        colActivated = 0;

        for (int y = 0; y < 4; ++y) {
            if (y == j) {
                colActivated = 1;
                continue;
            }

            cofactor.data[x - rowActivated][y - colActivated] = mat.data[x][y];
        }

        rowActivated = 0;
    }


    return ((i + j) % 2 == 0) ? Mat3_Det(cofactor) : -Mat3_Det(cofactor);
}


void Mat4_printMatrix(Mat4 matrix) {
    for (auto &i: matrix.data) {
        for (float j: i) {
            Serial.print(j, 2);
            Serial.print(" ");
        }
        Serial.println("");
    }
    Serial.println("");
}
