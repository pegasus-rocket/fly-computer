#include "vector.h"
#include "matrix.h"
#include "Arduino.h"

float Vec3_dot(const Vec3 *v1, const Vec3 *v2) {
    return v1->x * v2->x + v1->y * v2->y + v1->z * v2->z;
}

Vec3 *Vec3_normalize(const Vec3 *v) {
    Vec3 *res = Vec3_Set(0, 0, 0);
    const double magnitude = Vec3_magnitude(v);

    res->x = v->x / static_cast<float>(magnitude);
    res->y = v->y / static_cast<float>(magnitude);
    res->z = v->z / static_cast<float>(magnitude);

    return res;
}

double Vec3_magnitude(const Vec3 *v) {
    return sqrt(v->x * v->x + v->y * v->y + v->z * v->z);
}

Vec3 *Vec3_rotateVectorX(const Vec3 *v, const float angle) {
    Mat3 rX;

    rX.data[0][0] = 1;
    rX.data[0][1] = 0;
    rX.data[0][2] = 0;

    rX.data[1][0] = 0;
    rX.data[1][1] = cos(angle);
    rX.data[1][2] = -sin(angle);

    rX.data[2][0] = 0;
    rX.data[2][1] = sin(angle);
    rX.data[2][2] = cos(angle);

    return Mat3_MulMV(rX, v);
}

Vec3 *Vec3_rotateVectorY(const Vec3 *v, const float angle) {
    Mat3 rY;

    rY.data[0][0] = cos(angle);
    rY.data[0][1] = 0;
    rY.data[0][2] = sin(angle);

    rY.data[1][0] = 0;
    rY.data[1][1] = 1;
    rY.data[1][2] = 0;

    rY.data[2][0] = -sin(-angle);
    rY.data[2][1] = 0;
    rY.data[2][2] = cos(angle);

    return Mat3_MulMV(rY, v);
}

Vec3 *Vec3_rotateVectorZ(const Vec3 *v, const float angle) {
    Mat3 rZ;

    rZ.data[0][0] = cos(angle);
    rZ.data[0][1] = -sin(angle);
    rZ.data[0][2] = 0;

    rZ.data[1][0] = sin(angle);
    rZ.data[1][1] = cos(angle);
    rZ.data[1][2] = 0;

    rZ.data[2][0] = 0;
    rZ.data[2][1] = 0;
    rZ.data[2][2] = 1;

    return Mat3_MulMV(rZ, v);
}


void Vec3_print(const Vec3 *v) {
    Serial.print("x: ");
    Serial.print(v->x);
    Serial.print(", y: ");
    Serial.print(v->y);
    Serial.print(", z: ");
    Serial.println(v->z);
}

void Vec4_print(const Vec4 *v) {
    Serial.print("x: ");
    Serial.print(v->x);
    Serial.print(", y: ");
    Serial.print(v->y);
    Serial.print(", z: ");
    Serial.print(v->z);
    Serial.print(", w: ");
    Serial.println(v->w);
}

Vec4 *Vec4_eulerToQua(const Vec3 *v) {
    return Vec4_Set(v->x, v->y, v->z, 0.f);
}

double Vec4_magnitude(const Vec4 *v) {
    return sqrt(v->x * v->x + v->y * v->y + v->z * v->z + v->w * v->w);
}

float Vec4_dot(const Vec4 *v1, const Vec4 *v2) {
    return v1->x * v2->x + v1->y * v2->y + v1->z * v2->z + v1->w * v2->w;
}

Vec3 *Vec4_quaToEuler(const Vec4 *v) {
    const auto res = new Vec3{
        (atan2(2 * (v->w * v->x + v->y * v->z), 1 - 2 * (v->x * v->x + v->y * v->y))),
        (asin(2 * (v->w * v->y - v->z * v->x))),
        (atan2(2 * (v->w * v->z + v->x * v->y), 1 - 2 * (v->y * v->y + v->z * v->z))),
    };

    return res;
}

Vec4 *Vec4_normalize(const Vec4 *v) {
    Vec4 *res = Vec4_Set(0, 0, 0, 0);
    const double magnitude = Vec4_magnitude(v);

    res->x = v->x / static_cast<float>(magnitude);
    res->y = v->y / static_cast<float>(magnitude);
    res->z = v->z / static_cast<float>(magnitude);
    res->w = v->w / static_cast<float>(magnitude);

    return res;
}
