#include "main.h"

#include "localStorage.h"
#include "radio.h"
#include "sound.h"
#include <Wire.h>
#include "imu.h"
#include "fsLog.h"
#include "storage.h"
#include "Rocket.h"
#include "gnss.h"
#include "settings.h"
#include "barometer.h"
#include "logger.h"

#if ENABLE_WEB_SERVER

#include "webServer.h"
#include "WiFi.h"

WebServer server;
#endif

#ifdef DEVELOPMENT_ENABLED
Serial_debug debug;
#endif

Rocket rocket = Rocket_new();
Telemetry telemetryData;
Storage storage;
Radio radio;

BMP bmp;
IMU imu;
Gnss gnss;

constexpr word batteryTest = A1;

constexpr int parachute = 2; // Parachute Pin
constexpr int parachuteTest = A0; // Parachute test continuity pin

unsigned long telemetryTimeMemory = millis();
unsigned long telemetry_logTimeMemory = millis();
unsigned long statusTimeMemory = millis();
unsigned long parachuteFiredTime = millis();

unsigned long updateTimeMemory = millis();

void setup() {
    updateStatus();
    Serial.begin(SERIAL_BAUD_RATE);
    Serial.setTimeout(20);
    Sl.setLogLevel(DEBUG);
    Sl.enableLevelName(true);

    delay(500);

    if (!ls::LocalStorage::begin()) systemLog.append(ERROR, "Failed to initialize Local Storage.");
    delay(5);
    systemLog.append(INFO, "System is starting...");

    pinMode(parachute, OUTPUT);
    pinMode(BUZZER_PIN, OUTPUT);

    digitalWrite(parachute, LOW);

#if ENABLE_WEB_SERVER
    systemLog.append("Web Server is enabled.");
#if WIFI_ACCESS_POINT
    systemLog.append("Starting Wifi as access point");
    if (WiFi.softAP(WIFI_SSID, WIFI_PASSWORD)) {
        delay(100);
        IPAddress IP
        IPAddress MASK
        WiFi.softAPConfig(Ip, Ip, NMask);

        systemLog.append("AP IP address : " + WiFi.softAPIP().toString());
    } else {
        systemLog.append("WIFI AP creation failed.");
    }
#endif

#if !WIFI_ACCESS_POINT
    systemLog.append("Connecting to wifi.");
    WiFi.begin(WIFI_SSID, WIFI_PASSWORD);
    int i = 0;
    while (WiFiClass::status() != WL_CONNECTED) {
        delay(WIFI_TIMEOUT / 5);
        i++;
        if (i > 5) {
            systemLog.append("Can't connect to the Wifi");
            break;
        }
    }
    if (WiFiClass::status() == WL_CONNECTED) {
        systemLog.append("Wifi now connected to " + String(WIFI_SSID));
        systemLog.append("AP IP address : " + WiFi.localIP().toString());
        Sprintln("AP IP address : " + WiFi.localIP().toString());
    }
#endif
    server.add("/www/index.html", false, false, "/");
    server.add("/www", true, false);
    server.add("/log.txt", false, true, "/system-logs/download");
    server.add("/data.txt", false, true, "/fly-data/download");
    server.add("/log.txt", false, false, "/system-logs");
    server.add("/data.txt", false, false, "/fly-data");
    server.begin();
#endif
    // Init the accelerometer & gyroscope
    rocket.devices.imu.status = imu.init() ? READY : DOWN;
    delay(50);

    // Init the Barometric Pressure
    rocket.devices.bmp.status = bmp.begin() ? READY : DOWN;
    bmp.calibrate();

    rocket.devices.radio.status = radio.begin() == 0 ? READY : DOWN;
    radio.addEventListener<RocketAction>(Radio::ROCKET_ACTION, onRocketAction);

    delay(50);
    rocket.devices.gnss.status = gnss.begin() ? READY : DOWN;

    rocket.devices.parachute.status = isParachutePayloadConnected() ? READY : DOWN;

    delay(50);
    rocket.devices.system.status = BOOTED;
    systemLog.append(INFO, "System started");
}

void loop() {
    radio.update();
    rocket.devices.imu.status = imu.update() ? READY : DOWN;
    bmp.update();
    updateSensor();
    gnss.update();
    readSerial();
    playSound(); // Play buzzer sound

    if (millis() - statusTimeMemory >= SET_STATUS_INTERVAL) {
        updateStatus();
        statusTimeMemory = millis();
    }

    // Send telemetry to the base
    if (millis() - telemetryTimeMemory >= TELEMETRY_INTERVAL) {
        sendTelemetry();
        telemetryTimeMemory = millis();
    }

    // Save telemetry into local storage
    if (millis() - telemetry_logTimeMemory >= TELEMETRY_INTERVAL * 2 && rocket.flyState != ON_GROUND) {
        telemetry_logTimeMemory = millis();
        // flyData.append(rocket.toJSON().c_str());
    }

    // // Parachute deployment
    // if (Vec4_quaToEuler(rocket.orientation)->x > 0) {
    //     // The rocket is pointing down.
    //     // If (!rocketStatus.parachuteFired) {
    //     releaseParachute();
    //     // }
    // }

    // Set the parachute pin to low 2 s after deployment.
    if (rocket.devices.parachute.status == FIRED && millis() - parachuteFiredTime > 2000)
        digitalWrite(parachute, LOW);

    // Emergency cases
    // TODO: If IMU connection lost then trigger the parachute
}

void sendTelemetry() {
    radio.sendMessage(new Radio::RadioMessage{RADIO_BASE_ADDRESS, &rocket, sizeof(Rocket), Radio::ROCKET_DATA});

#ifdef DEVELOPMENT_ENABLED
    debug.rocket = &rocket;
    Serial.println(debug.toJSON().c_str());
#endif
}

void updateSensor() {
    // Accelerometer
    rocket.orientation = *imu.getOrientation();
    rocket.acceleration = *imu.getAccel();
    rocket.velocity = *imu.getVelocity();

    // GNSS
    rocket.longitude = gnss.longitude;
    rocket.latitude = gnss.latitude;

    if (millis() - updateTimeMemory >= 500) {
        updateTimeMemory = millis();
        // Altimeter
        rocket.altitude = bmp.getAltitude();

        // Battery
        const long batteryLevel = ::map(analogRead(batteryTest), 1500, 2100, 0, 100);
        rocket.devices.batteryLevel = min(max(batteryLevel, 0L), 100L);
    }
}

void updateStatus() {
    if (rocket.devices.system.status != BOOTING) {
        rocket.devices.system.status = READY;

        if (
            rocket.devices.imu.status == DOWN ||
            rocket.devices.bmp.status == DOWN ||
            rocket.devices.radio.status == DOWN ||
            rocket.devices.parachute.status == DOWN
        )
            rocket.devices.system.status = DOWN;
    }

    if (rocket.devices.system.status == BOOTING)
        return updateLedStatus(ORANGE);

    if (rocket.devices.system.status == DOWN) {
        changeSound(ERROR_SOUND);
        return updateLedStatus(RED);
    }
    changeSound(READY_SOUND);
    updateLedStatus(GREEN);
}

void updateLedStatus(LedColor color) {
    analogWrite(LED_RED, 255); // Turn off lights
    analogWrite(LED_GREEN, 255);
    analogWrite(LED_BLUE, 255);

    switch (color) {
        case GREEN:
            analogWrite(LED_GREEN, LOW);
            break;
        case RED:
            analogWrite(LED_RED, LOW);
            break;
        case PURPLE:
            analogWrite(LED_RED, LOW);
            analogWrite(LED_BLUE, LOW);
            break;
        case BLUE:
            analogWrite(LED_BLUE, LOW);
            break;
        case WHITE:
            analogWrite(LED_RED, LOW);
            analogWrite(LED_BLUE, LOW);
            analogWrite(LED_GREEN, LOW);
            break;
        case YELLOW:
            analogWrite(LED_RED, LOW);
            analogWrite(LED_GREEN, LOW);
            break;
        case ORANGE:
            analogWrite(LED_RED, LOW);
            analogWrite(LED_GREEN, 255 - 165);
            break;
        default:
            break;
    }
}

bool isParachutePayloadConnected() {
    digitalWrite(parachute, LOW);
    return analogRead(parachuteTest) > 1000;
}

void releaseParachute() {
    if (rocket.devices.parachute.status == READY && rocket.devices.system.status == READY) {
        parachuteFiredTime = millis();
        digitalWrite(parachute, HIGH);
        rocket.devices.parachute.status = FIRED;
        systemLog.append(DEBUG, "Parachute deployed!");
    } else
        systemLog.append(DEBUG, "Parachute deployment cancelled.");
}

void readSerial() {
    if (Serial && Serial.available() > 0) {
        String str = Serial.readString(); // read until timeout
        str.trim();

        if (str == "altitude") {
            Serial.print(F("Maximum altitude recorded : "));
            Storage::printItem(STORAGE_ALTITUDE);
        } else if (str == "log print system") {
            Serial.println(F("Reading system log"));
            systemLog.printLog();
        } else if (str == "log print fly-data") {
            Serial.println(F("Reading fly-data log"));
            flyData.printLog();
        } else if (str == "log delete system") {
            Serial.println(F("System log is going to be deleted..."));
            systemLog.deleteLogs();
            Serial.println(F("System log deleted!"));
        } else if (str == "log delete fly-data") {
            Serial.println(F("Fly Data is going to be deleted..."));
            flyData.deleteLogs();
            Serial.println(F("Fly data deleted!"));
        } else if (str == "format fs") {
            Serial.println(F("FS is going to be formated..."));
            ls::LocalStorage::format();
            Serial.println(F("FS is formated!"));
        } else {
            Serial.println(F("\nCommands available :"));
            Serial.println(F("\t altitude : \tGet the maximum altitude recorded"));
            Serial.println(F("\t log print system : \tPrint system logs"));
            Serial.println(F("\t log print fly-data : \tPrint fly's data"));
            Serial.println(F("\t log delete system : \tDelete system log"));
            Serial.println(F("\t log delete fly-data : \tDelete fly's data"));
            Serial.print("\n\n");
        }
    }
}

void onRocketAction(const RocketAction *action) {
    if (action->fireParachute) {
        systemLog.append(DEBUG, "Parachute deployed by Serial.");
        releaseParachute();
    }
    if (action->resetStorage) {
        Storage::clearStorage();
    }
}
