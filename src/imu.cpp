#include "imu.h"
#include "settings.h"
#include "fsLog.h"

BNO08x IMU::myIMU;

#define STATIC_ACCELERATION_THRESHOLD 0.35
#define STATIC_ANGULAR_VELOCITY_THRESHOLD 0.15
#define STATIC_SAMPLES_THRESHOLD 10

bool IMU::init() {
    bool success = true;
    success &= Wire.begin();

    if (!myIMU.begin(BNO08X_ADDR, Wire, BNO08X_INT, BNO08X_RST)) {
        systemLog.append(ERROR, "Error initializing BNO08X");
        return false;
    }

    success &= Wire.setClock(400000); // Increase I2C data rate to 400kHz

    delay(20);

    // Enables features, try again if the first attempt failed.
    for (int i = 0; i < 3; i++) {
        success &= enableFeatures();
        if (success) break;
        delay(20);
    }

    return success;
}

bool IMU::enableFeatures() {
    if (!myIMU.isConnected()) {
        Sl.debug("Could not connect to BNO08X.\n");
        return false;
    }
    systemLog.append(DEBUG, "BNO08X enable Features.");
    if (myIMU.enableRotationVector(1)) {
        systemLog.append(DEBUG, "Rotation vector enabled on BNO08X.");
    } else {
        systemLog.append(DEBUG, "Could not enable rotation vector on BNO08X.");
        return false;
    }

    if (myIMU.enableLinearAccelerometer(1)) {
        systemLog.append(DEBUG, "Acceleration vector enabled on BNO08X.");
    } else {
        systemLog.append(DEBUG, "Could not enable acceleration vector on BNO08X.");
        return false;
    }
    return true;
}

bool IMU::update() {
    if (!myIMU.isConnected()) {
        Sl.debug("Could not connect to BNO08X.\n");
        return false;
    }
    bool success = true;
    if (myIMU.wasReset()) success &= enableFeatures();
    if (success && millis() - lastUpdate >= 5) {
        lastUpdate = millis();

        if (myIMU.getSensorEvent()) {
            if (myIMU.getSensorEventID() == SENSOR_REPORTID_ROTATION_VECTOR) {
                orientation->x = myIMU.getQuatI();
                orientation->y = myIMU.getQuatJ();
                orientation->z = myIMU.getQuatK();
                orientation->w = myIMU.getQuatReal();
            }


            if (myIMU.getSensorEventID() == SENSOR_REPORTID_LINEAR_ACCELERATION) {
                acceleration->x = myIMU.getLinAccelX();
                acceleration->y = myIMU.getLinAccelY();
                acceleration->z = myIMU.getLinAccelZ();
            }
        }
    }
    return success;
}

bool IMU::zeroVelocityUpdate() {
    return false;
}

void IMU::integrate(float x1, float x2, float y1, float y2) {
}

Vec3 *IMU::getAccel() const {
    return acceleration;
}

Vec4 *IMU::getOrientation() const {
    return orientation;
}

Vec3 *IMU::getVelocity() const {
    return velocity;
}
