#include "webServer.h"

#include <utility>
#include "localStorage.h"

void WebServer::begin() {
    for (const ServerRules &rule: rules) {
        if (rule.isDirectory) {
            list<string> paths;
            localStorage.getFilesPath(rule.path.c_str(), FILES_LEVELS, &paths);

            for (const string &path: paths) {
                server.on(path.substr(4).c_str(), HTTP_GET, [path, this](AsyncWebServerRequest *request) {
                    String extension = ls::LocalStorage::getFileExtension(path).c_str();
                    String fileType = getFileType(extension);

                    request->send(localStorage.fs, path.c_str(), fileType, false);
                });
            }
        } else {
            if (rule.allowDownload) {
                server.on(rule.overwriteUrl, HTTP_GET, [rule](AsyncWebServerRequest *request) {
                    if (localStorage.exists(rule.path.c_str()))
                        request->send(localStorage.fs, rule.path, String(), true);
                    else
                        request->send(404, "text/plain", "No such file or directory");
                });
            } else {
                server.on(rule.overwriteUrl, HTTP_GET, [rule, this](AsyncWebServerRequest *request) {
                    if (localStorage.exists(rule.path.c_str())) {
                        String extension = ls::LocalStorage::getFileExtension(rule.path.c_str()).c_str();
                        String fileType = getFileType(extension);
                        AsyncWebServerResponse *response = request->beginResponse(localStorage.fs, rule.path, fileType);
                        request->send(response);
                    }
                    else
                        request->send(404, "text/plain", "No such file or directory");
                });
            }
        }
    }

    DefaultHeaders::Instance().addHeader("Access-Control-Allow-Origin", "*");
    server.begin();
}

void WebServer::add(
    String path,
    bool isDirectory,
    bool allowDownload,
    const char *overwriteUrl,
    const char *redirectUrl) {
    rules.emplace_back(ServerRules{std::move(path), isDirectory, allowDownload, overwriteUrl, redirectUrl});
}

String WebServer::getFileType(const String &extension) {
    if (extension == "css") return "text/css";
    if (extension == "js") return "application/javascript";
    if (extension == "txt") return "text/plain";
    return {};
}
