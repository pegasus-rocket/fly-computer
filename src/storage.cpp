#include "storage.h"
#include <EEPROM.h>
#include "settings.h"

void Storage::addItem(int key, int value) { EEPROM.put(key, value); }

void Storage::getItem(int key, int* value) { EEPROM.get(key, value); }

void Storage::printStorage() {
    for (int index = 0; index < EEPROM.length(); index++) {
        Storage::printItem(index);
    }
}

void Storage::printItem(int key) {
    byte value = EEPROM.read(key);
    Serial.print("Key : ");
    Serial.print(key);
    Serial.print("\tvalue : ");
    Serial.println(value, DEC);
}

void Storage::updateItem(int key, int value) {
//    EEPROM.update(key, value);
}

void Storage::clearStorage() {
    Sl.info("Cleaning storage, this could take a while...\n");
    for (int i = 0; i < 100; i++) {
        EEPROM.write(i, 0);
        Sl.info(".");
    }
    Sl.info("Storage cleaned\n");
}