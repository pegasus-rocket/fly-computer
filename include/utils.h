#pragma once
inline unsigned long getMillis() {
    return millis() % 100;
}

inline unsigned long getSecond() {
   return (millis() / 1000) % 60;
}

inline unsigned long getMinutes() {
    return ((millis() / 1000) / 60) % 60;
}

inline unsigned long getHours() {
    return ((millis() / 1000) / (60 * 60)) % 24;
}