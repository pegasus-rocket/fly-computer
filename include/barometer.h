#pragma once
#include <Adafruit_Sensor.h>
#include "Adafruit_BMP3XX.h"
#include "fsLog.h"

class BMP {
    Adafruit_BMP3XX bmp;
    float tare = 0;
    unsigned long lastUpdate = 0;
public:
    bool begin() {
        if (bmp.begin_I2C()) {
            bmp.setPressureOversampling(BMP3_NO_OVERSAMPLING);
            bmp.setIIRFilterCoeff(BMP3_IIR_FILTER_COEFF_3);
            bmp.setOutputDataRate(BMP3_ODR_12_5_HZ);
            return true;
        }

        return false;
    }

    void update(const int interval = 500) {
        if (millis() - lastUpdate > interval) {
            lastUpdate = millis();
            bmp.performReading();
        }
    }

    void calibrate() {
        float average_altitudes = 0.f;
        for (int i = 0 ; i < 10 ; i++) {
            delay(85);
            update(80);
            average_altitudes += getAltitude();
        }
        average_altitudes /= 10;
        tare = average_altitudes;
        systemLog.append(INFO, "Altitude tare : %f", tare);
    }

    float getAltitude() {
        const float altitude = bmp.readAltitude(SEALEVELPRESSURE_HPA) - tare;
        if (altitude > -0.2 && altitude < 0.2) return 0.f;
        return altitude;
    }

    double getTemperature() const {
        return bmp.temperature;
    }

    double getPressure() const {
        return bmp.pressure;
    }
};
