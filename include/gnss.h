#pragma once
#include <SparkFun_u-blox_GNSS_v3.h>

class Gnss {
public:
    long longitude = 0;
    long latitude = 0;

    bool begin() {
        return
                GNSS.begin() &&
                GNSS.setI2COutput(COM_TYPE_UBX) &&
                GNSS.setNavigationFrequency(1) &&
                GNSS.setAutoPVTcallbackPtr(nullptr);
    }

    void update() {
        if (!GNSS.isConnected()) {
            Sl.debug("GNSS is not connected\n");
            return;
        }
        GNSS.checkUblox();

        if (GNSS.packetUBXNAVPVT->automaticFlags.flags.bits.callbackCopyValid) {
            // Prevent the callback from being called.
            GNSS.packetUBXNAVPVT->automaticFlags.flags.bits.callbackCopyValid = false;

            latitude = GNSS.packetUBXNAVPVT->callbackData->lat;
            longitude = GNSS.packetUBXNAVPVT->callbackData->lon;
        }
    }

private:
    SFE_UBLOX_GNSS GNSS;
};
