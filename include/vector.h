#pragma once
#include <cstdint>


//-------------------------------------------------------------------------------------------------
// Structure Vec3

typedef union Vec3_telemetry_u {
    struct {
        int32_t x;
        int32_t y;
        int32_t z;
    };

    int32_t data[3];
} Vec3_telemetry;

typedef union Vec3_u {
    struct {
        float x;
        float y;
        float z;
    };

    float data[3];
} Vec3;

/// Compute the dot product between two vectors.
/// \param v1 The first vector.
/// \param v2 The second vector.
/// \return The dot product between v1 and v2.
float Vec3_dot(const Vec3 *v1, const Vec3 *v2);

/// \brief Set x, y, z values to a vector.
/// \param x element x
/// \param y element y
/// \param z element w
/// \return Vec3
inline Vec3 *Vec3_Set(float x, float y, float z) {
    return new Vec3{x, y, z};
}

/// Normalize a vector by divided each element by the length of the vector.
/// \param v The vector to normalize.
/// \return Normalized vector.
Vec3 *Vec3_normalize(Vec3 v);

/// Get the length of a vector.
/// \param v The vector.
/// \return The length of the vector.
double Vec3_magnitude(const Vec3 *v);

void Vec3_print(const Vec3 &v);

/// Rotate given vector with a given angle.
/// \param v Vector to rotate.
/// \param angle
/// \return rotated vector.
Vec3 *Vec3_rotateVectorX(const Vec3 *v, float angle);

/// Rotate given vector with a given angle.
/// \param v Vector to rotate.
/// \param angle
/// \return rotated vector.
Vec3 *Vec3_rotateVectorY(const Vec3 *v, float angle);

/// Rotate given vector with a given angle.
/// \param v Vector to rotate.
/// \param angle
/// \return rotated vector.
Vec3 *Vec3_rotateVectorZ(const Vec3 *v, float angle);

//-------------------------------------------------------------------------------------------------
// Structure Vec4

typedef union Vec4_telemetry_u {
    struct {
        int32_t x;
        int32_t y;
        int32_t z;
        int32_t w;
    };

    int32_t data[4];
} Vec4_telemetry;

typedef union Vec4_u {
    struct {
        float x;
        float y;
        float z;
        float w;
    };

    float data[4];
} Vec4;

/// Set x, y, z and w values to a vector.
/// \param x element x
/// \param y element y
/// \param z element z
/// \param w element w
/// \return Vec4
inline Vec4 *Vec4_Set(float x, float y, float z, float w) {
    return new Vec4{x, y, z, w}; // Allocates memory for Vec4 on heap
}

/// Normalize a vector by divided each element by the length of the vector.
/// \param v The vector to normalize.
/// \return Normalized vector.
Vec4 Vec4_normalize(Vec4 v);

/// Get the length of a vector.
/// \param v The vector.
/// \return The length of the vector.
double Vec4_magnitude(const Vec4 &v);

/// Compute the dot product between two vectors.
/// \param v1 The first vector.
/// \param v2 The second vector.
/// \return The dot product between v1 and v2.
float Vec4_dot(Vec4 v1, Vec4 v2);

/// Convert euler vector to quaternion.
/// \param v The euler vector.
/// \return Quaternion vector.
Vec4 *Vec4_eulerToQua(const Vec3 *v);

Vec3 *Vec4_quaToEuler(const Vec4 *v);

/// Print the vector through Serial.
/// \param v The matrix to print.
void Vec4_print(Vec4 v);
