#pragma once
#include "Arduino.h"
#include "vector.h"
#include "matrix.h"
#include <atomic>
#include "logger.h"

using namespace std;

// This file contains all the rocket's settings.

// --- GENERAL SETTINGS ---

// Serial baud rate
#define SERIAL_BAUD_RATE 500000

// Time interval in milliseconds to check the rocket's device status.
#define SET_STATUS_INTERVAL 3000

// When uncommented, the rocket object will be print through serial. Comment the line below to disable debug mod.
// #define DEVELOPMENT_ENABLED

// Set default sea level pressure for better accuracy.
#define SEALEVELPRESSURE_HPA (1009)

// --- IMU ---

#define BNO08X_INT  5
#define BNO08X_RST  6
#define BNO08X_ADDR 0x4B

// --- RADIO ---
// The section below is about radio's settings.

// Time interval in milliseconds to send the rocket's telemetry.
#define TELEMETRY_INTERVAL 80

#define RADIO_MAX_PAYLOAD_SIZE 80

#define RADIO_PIN_CS 10
// #define RADIO_PIN_IRQ 2
#define RADIO_PIN_IRQ 4
#define RADIO_PIN_RST 3
#define RADIO_PIN_GPIO 9

#define RADIO_FREQ 868.3
#define RADIO_BR 15.0
#define RADIO_FREQ_DEV 57.136417
#define RADIO_RXBW 234.3
#define RADIO_POWER 10
#define RADIO_PREAMBLE_LENGTH 32

// Radio rocket address
#define RADIO_ROCKET_ADDRESS 01

// Base rocket address
#define RADIO_BASE_ADDRESS 00
#define RADIO_CHANNEL 90


// --- WIFI ---

// When set to true, the web server will be initialized. The web page is hosted on the Fly-Computer itself
// it allows to download logs, see device status and get fly videos.
#define ENABLE_WEB_SERVER false

// When web server is set to true, the fly-computer will either create an access point or connect to set network.
// When is set to true, the fly-computer will create an access point
// if set to false will attempt to connect to the defined network.
// If true, the default ip is 192.168.1.1. This can be edited in the IP section.
#define WIFI_ACCESS_POINT true

// If WIFI_ACCESS_POINT is set to true, this will be the name of the access point.
// Otherwise, the fly-computer will try to connect to the network.
#define WIFI_SSID ""

// If WIFI_ACCESS_POINT is set to true, it will be the password to allow a device connection.
// Otherwise, it will be the password used to connect the fly-computer to the defined network.
#define WIFI_PASSWORD ""

// Time in milliseconds before the fly-computer stop to try to connect to the defined network.
#define WIFI_TIMEOUT 4000

// Only used when WIFI_ACCESS_POINT is true.
// This setting allows setting a default IP to the fly-computer if it works as an access point.
#define IP Ip(192, 168, 1, 1);

// Only used when WIFI_ACCESS_POINT is true.
#define MASK NMask(255, 255, 255, 0);


// --- STORAGE ---

#define ENABLE_LOCAL_STORAGE false

// The local storage is manage by the SPIFFS lib. If SPIFFS file system mount fails, then it will be formatted.
// Note that if set to true, data can be permanently deleted.
#define FORMAT_FS_IF_FAILED false

// System log path.
#define SYSTEM_LOG_PATH "/log.txt"

// Data log path.
#define FLY_DATA_LOG_PATH "/data.txt"


// --- SENSOR_TIME ---

#define LOW_PRIORITY_SENSOR_INTERVAL 1000

#define MIDDLE_PRIORITY_SENSOR_INTERVAL 15