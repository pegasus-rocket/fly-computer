#pragma once

#include <sstream>
#include <iomanip>

#define FLOAT_PRECISION 4

// Use to formatting JSON data. See code below.
enum Format {
    AUTO, CURLY_BRACKET, QUOTE
};

/// Check if custom type is a string
template<typename T>
struct is_string : std::false_type {
};

template<>
struct is_string<std::string> : std::true_type {
};

/// Serialize data to JSON with data as array.
/// \tparam T Type of data, can be an array of : int, float, double, char, string...
/// \tparam N Number of element in the array.
/// \param data The data array.
/// \param key The main JSON key where to stored data.
/// \param subKeys The JSON sub keys to store data values.
/// \return The JSON as a string.
template<typename T, size_t N>
std::string serializeToJson(T (&data)[N], const char *key, const char *subKeys[]) {
    std::ostringstream json;
    json << R"(")" << key << R"(": {)";
    const char *separator = "";
    // Iterate over the members of the struct
    for (size_t i = 0; i < N; ++i) {
        json <<
             separator <<
             "\"" <<
             subKeys[i] <<
             "\":" <<
             std::setprecision(FLOAT_PRECISION)
             << data[i];
        separator = ", ";
    }
    json << "}";

    return json.str();
}

/// Serialize data to JSON with data as single value.
/// \tparam T Type of data, can be : int, float, double, char, string...
/// \param data The data to serialize to JSON.
/// \param key The me JSON key where to store DATA.
/// \param format Used to know how to format data value.
///               Ex: Surround data value with: ', ", (, [, {
///               Possible value:
///               AUTO: Auto formatting value
///               CURLY_BRACKET: Surround data with {}
/// \return The JSON as a string.
template<typename T>
std::string serializeToJson(T data, const char *key, Format format = AUTO) {
    std::ostringstream json;
    json << R"(")" << key << R"(": )";

    // Add start format
    switch (format) {
        case AUTO:
            if (is_string<decltype(data)>::value) {
                json << "\"";
            }
            break;
        case CURLY_BRACKET:
            json << "{";
            break;
        case QUOTE:
            json << "\"";
            break;
        default:
            break;
    }

    json << std::setprecision(FLOAT_PRECISION) << data;

    // Add end format
    switch (format) {
        case AUTO:
            if (is_string<decltype(data)>::value) {
                json << "\"";
            }
            break;
        case CURLY_BRACKET:
            json << "}";
            break;
        case QUOTE:
            json << "\"";
            break;
        default:
            break;
    }

    return json.str();
}
