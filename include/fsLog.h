#pragma once
#include "settings.h"

namespace logging {
    class Logging {
    public:
        explicit Logging(const char * path) : log_path(path) {
            xTaskCreatePinnedToCore(taskWrapper, "Task Blink", 8000, this, 1, nullptr, 0);
        }

        void append(LogLevel logLevel, const char * format, ...) const;
        bool deleteLogs() const;
        void printLog() const;
        void flush() const;

    private:
        static void taskWrapper(void *pvParameters);
        const char *log_path;
        mutable String buffer = "";
    };
}

extern logging::Logging systemLog;
extern logging::Logging flyData;
