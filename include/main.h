#pragma once

enum LedColor {
    RED,
    GREEN,
    PURPLE,
    BLUE,
    WHITE,
    YELLOW,
    ORANGE
};

typedef struct RocketAction_s {
    bool fireParachute;
    bool resetStorage;
} __attribute__((packed, aligned(1))) RocketAction;

/**
 * @brief Send telemetry data to the base.
 *
 * This function sends telemetry data using NRF24L01+.
 * It populates the telemetryData structure with the current acceleration,
 * rocketStatus, and gyro values.
 */
void sendTelemetry();


/**
 * @brief Update sensor data.
 *
 * This function updates the acceleration and gyro data by reading values from
 * the MPU sensor. It uses the Adafruit MPU6050 library (assuming 'mpu' is an
 * instance of that library). The updated values are stored in the
 * 'acceleration' and 'gyro' variables.
 */
void updateSensor();

/**
 * Update the status of the rocket and control indicators (LEDs and sound).
 * This function assesses the current status of the rocket and performs the
 * following actions:
 * 1. Sets the rocket's readiness to fly as 'true' by default.
 * 2. Checks if the rocket is in the booting state. If so, it logs "BOOTING" and
 * sets readiness to 'false'.
 * 3. Checks if the MPU (Motion Processing Unit) is in an error state. If so, it
 * logs "MPU ERROR !!" and sets readiness to 'false'.
 * 4. Checks if the radio communication is in an error state. If so, it logs
 * "RADIO ERROR !!" and sets readiness to 'false'.
 * 5. Depending on the rocket's status, it updates the LED indicators and sound
 * output accordingly:
 *    - If the rocket is in the booting state, it sets the LED to ORANGE.
 *    - If the rocket is not ready to fly (due to booting, MPU error, or radio
 * error), it sets the LED to RED and plays an error sound.
 *    - If none of the above conditions are met, it sets the LED to GREEN and
 * plays a ready sound.
 */
void updateStatus();

/**
 * Update the status LED indicator based on the specified color.
 * This function controls the status LED by setting its color based on the input
 * parameter.
 *
 * @param color A enum element representing the desired LED color.
 *              - GREEN: Sets the status LED to green.
 *              - RED: Sets the status LED to red.
 *              - ORANGE: Sets the status LED to orange (a combination of green
 * and red).
 *              - BLUE: Sets the status LED to blue.
 *              Other color values will turn off all LED colors.
 */
void updateLedStatus(LedColor color);

/**
 * Test current continuity for the parachute deployment system.
 * This function checks if there is proper electrical continuity for the
 * parachute system. It sets the rocket's parachute readiness status to 'OK' if
 * the continuity is good, or 'KO' if there's an issue.
 *
 * @return 'OK' if continuity is good, 'KO' if there's an issue.
 */
bool isParachutePayloadConnected();

void releaseParachute();

void readSerial();

void readNetwork();

/**
 * This callback function is executed when a message of type RocketAction is receive by the radio.
 * @param action
 */
void onRocketAction(const RocketAction *action);