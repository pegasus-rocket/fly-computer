#pragma once
#include "settings.h"
#include "json.h"

/**
 * The different status of a device:
 * - DOWN: The device is down and is not usable.
 * - READY: The device is up and init. Everything is OK.
 * - SLEEP: The device is init but data can't be fetched. The device is in low consumption mode.
 * - BOOTING: Sate during the setup of the device.
 * - CONNECTED: When the device is ready and connected to another device. Ex: Radio, bluetooth...
 * - FIRED: Specific to the parachute or motor.
 * - BOOTED: State just after the device is booting.
 */
enum __attribute__((packed)) DeviceStatus {
    DOWN,
    READY,
    SLEEP,
    BOOTING,
    CONNECTED,
    FIRED ,
    BOOTED
};

/**
 * The rocket's different states of flight.
 * - FLYING: Specific to the rocket. The rocket is flying.
 * - LANDED: Specific to the rocket. The rocket is landed (After a fly).
 * - ON_GROUND: Specific to the rocket. The rocket is on the ground (before a fly).
 */
enum __attribute__((packed)) FlyState {
    FLYING,
    LANDED,
    ON_GROUND
};

/// Get device status string
/// \param status status to convert to string
/// \return The device status as string
inline std::string getStatus(const DeviceStatus status) {
    switch (status) {
        case DOWN:
            return "down";
        case READY:
            return "ready";
        case SLEEP:
            return "sleep";
        case CONNECTED:
            return "connected";
        case FIRED:
            return "fired";
        case BOOTED:
            return "booted";
        default:
            Serial.println("Unimplemented DeviceStatus item");
            return "";
    }
}

/**
 * Get the rocket's flight status.
 * @param status The status to get into a string
 * @return The status as a string
 */
inline std::string getStatus(const FlyState status) {
    switch (status) {
        case FLYING:
            return "flying";
        case LANDED:
            return "landed";
        case ON_GROUND:
            return "on_ground";
        default:
            Serial.println("Unimplemented FlyState item");
            return "";
    }
}

/**
 * Device structure
 */
typedef struct Device_s {
    DeviceStatus status;
#ifdef DEVELOPMENT_ENABLED

    // Return the structure data to JSON.
    [[nodiscard]] std::string toJSON(bool isKey = false) const {
        std::ostringstream json;

        if (!isKey) json << "{";

        // Serialize device to JSON
        json << serializeToJson(getStatus(status), "status");

        if (!isKey) json << "}";

        return json.str();
    }

#endif
} __attribute__((packed, aligned(1))) Device;


/**
 * @brief Rocket devices structure.
 *
 * This structure defines the rocket's devices.
 * .toJSON can be called to get a JSON string of the structure.
 *
 * Device description:
 *  - radio: The radio device used to communicate with the base.
 *  - parachute: The way used to slow down the speed of the rocket.
 *  - bmp: The barometer to have the altitude.
 *  - system: This is the global state of the rocket.
 *  It is determinate with the other device.
 *  - batteryLevel: The battery of the computer.
 */
typedef struct Devices_s {
    Device radio;
    Device parachute;
    Device bmp;
    Device imu;
    Device system;
    Device gnss;
    uint8_t batteryLevel;

#ifdef DEVELOPMENT_ENABLED

    // Return the structure data to JSON.
    [[nodiscard]] std::string toJSON(bool isKey = false) const {
        std::ostringstream json;

        if (!isKey) json << "{";

        // Serialize status to JSON
        json << serializeToJson(radio.toJSON(true), "radio", CURLY_BRACKET) << ",";
        json << serializeToJson(parachute.toJSON(true), "parachute", CURLY_BRACKET) << ",";
        json << serializeToJson(bmp.toJSON(true), "bmp", CURLY_BRACKET) << ",";
        json << serializeToJson(imu.toJSON(true), "imu", CURLY_BRACKET) << ",";
        json << serializeToJson(system.toJSON(true), "system", CURLY_BRACKET) << ",";
        json << serializeToJson(static_cast<float>(batteryLevel), "batteryLevel");

        if (!isKey) json << "}";

        return json.str();
    }

#endif
} __attribute__((packed, aligned(1))) Devices;


/**
 * This structure contains all the information about the rocket.
 * .toJSON can be called to get the JSON string of the structure.
 */
typedef struct Rocket_s {
    Vec4 orientation;
    Vec3 velocity;
    Vec3 acceleration;
    float altitude;
    FlyState flyState;
    Devices devices;
    long latitude;
    long longitude;
    bool emergency;

#ifdef DEVELOPMENT_ENABLED

    // Return the structure data to JSON.
    [[nodiscard]] std::string toJSON(bool isKey = false) const {
        std::ostringstream json;

        if (!isKey) json << "{";

        // Serialize orientation to JSON
        const char *Vec4[] = {"x", "y", "z", "w"};
        const char *Vec3[] = {"x", "y", "z"};
        json << serializeToJson(getStatus(flyState), "flyState") << ",";
        json << serializeToJson(devices.toJSON(true), "devices", CURLY_BRACKET) << ",";
        json << serializeToJson(emergency, "emergency") << ",";
        json << serializeToJson(altitude, "altitude") << ",";
        json << serializeToJson(latitude, "latitude") << ",";
        json << serializeToJson(longitude, "longitude") << ",";
        json << serializeToJson(orientation.data, "orientation", Vec4) << ",";
        json << serializeToJson(acceleration.data, "acceleration", Vec3) << ",";
        json << serializeToJson(velocity.data, "velocity", Vec3);

        if (!isKey) json << "}";

        return json.str();
    }

#endif
} __attribute__((packed, aligned(1))) Rocket;

/**
 * Set the default value to the rocket.
 * @return Rocket object.
 */
inline Rocket Rocket_new() {
    Rocket rocket;
    rocket.orientation.x = 0;
    rocket.orientation.y = 0;
    rocket.orientation.z = 0;
    rocket.orientation.w = 0;
    rocket.velocity.x = 0;
    rocket.velocity.y = 0;
    rocket.velocity.z = 0;
    rocket.acceleration.x = 0;
    rocket.acceleration.y = 0;
    rocket.acceleration.z = 0;
    rocket.altitude = 0;
    rocket.emergency = false;
    rocket.longitude = 0;
    rocket.latitude = 0;
    rocket.flyState = ON_GROUND;
    rocket.devices.system.status = DOWN;
    rocket.devices.bmp.status = DOWN;
    rocket.devices.gnss.status = DOWN;
    rocket.devices.imu.status = DOWN;
    rocket.devices.radio.status = DOWN;
    rocket.devices.parachute.status = DOWN;
    rocket.devices.system.status = DOWN;
    rocket.devices.batteryLevel = 0;
    return rocket;
}

/**
 * @brief Telemetry structure.
 *
 * This structure defines a telemetry packet that contains sensor data and
 * rocket status. It includes the current acceleration, gyro data, and rocket
 * status information.
 * __attribute__((packed, aligned(1))): https://forum.arduino.cc/t/data-transmision-issue-from-arduino-pro-mini-to-esp32-via-nrf24l01/918278
 */
typedef struct Telemetry_s {
    Vec4_telemetry orientation;
    Vec3_telemetry velocity;
    Vec3_telemetry acceleration;
    int32_t altitude;
    FlyState flyState;
    Devices devices;
    bool emergency;
} Telemetry;

/**
 * JSON structure printed in when debug mode is enabled.
 */
#ifdef DEVELOPMENT_ENABLED

typedef struct Serial_debug_s {
    Rocket *rocket;
    Vec3 Vec3_debugVectors[3];

    /// Return the structure data to JSON.
    [[nodiscard]] std::string toJSON() const {
        std::ostringstream json;

        json << "{";

        json << R"("device_type": "rocket",)";

        int counter = 0;
        for (auto &vector: Vec3_debugVectors) {
            const char *vec3[] = {"x", "y", "z"};
            std::string vectorName = "debug_vector" + std::to_string(counter++);
            json << serializeToJson(vector.data, vectorName.c_str(), vec3) << ",";
        }

        // Serialize status to JSON
        json << serializeToJson(rocket->toJSON(true), "rocket", CURLY_BRACKET);

        json << "}";

        return json.str();
    }

} Serial_debug;
#endif
