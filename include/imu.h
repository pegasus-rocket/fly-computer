#pragma once
#include "SparkFun_BNO08x_Arduino_Library.h"
#include "settings.h"

class IMU {
    Vec4 *orientation = Vec4_Set(0, 0, 0, 0);
    Vec3 *acceleration = Vec3_Set(0, 0, 0);
    Vec3 *velocity = Vec3_Set(0, 0, 0);

    // Track the time interval for each call of the update function.
    unsigned long lastUpdate = 0;

    static BNO08x myIMU;

    // Enable BNO08x's features. Ex: rotation vector, linear acceleration...
    static bool enableFeatures();

    static bool zeroVelocityUpdate();

    static void integrate(float x1, float x2, float y1, float y2);

public:
    /**
     * Init the IMU modules.
     * @return true if init went fine false if an error occurred.
     */
    static bool init();

    /**
     * Update IMU datas, need to be called soo as possible. With a delta < 10 ms.
     */
    bool update();

    /**
     * Get the instant linear acceleration in m/s^s.
     * @return Vec3 (x,y,z) linear acceleration.
     */
    Vec3 *getAccel() const;

    /**
     * Get the quaternion orientation.
     * @return Vec4 (x,y,z,w): x: I, y: J, z: K, w: Real
     */
    Vec4 *getOrientation() const;

    /**
     * Get the instant velocity in m/s.
     * @return Vec3 (x,y,z)
     */
    Vec3 *getVelocity() const;
};