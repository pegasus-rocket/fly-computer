#pragma once
#include <LittleFS.h>
#include <list>
#include "settings.h"

namespace ls {
    class LocalStorage : private fs::LittleFSFS {
    public:
        static bool begin() {
#if ENABLE_LOCAL_STORAGE
            return LittleFS.begin(FORMAT_FS_IF_FAILED);
#else
            Sl.info("Local storage is disabled.\n");
            return false;
#endif
        }

        static void format() {
            LittleFS.format();
        }

        void printDir(const char *dirname, uint8_t levels);

        void readFile(const char *path) const;

        bool writeFile(const char *path, const char *message) const;

        bool appendFile(const char *path, const String &message) const;

        void renameFile(const char *path1, const char *path2) const;

        size_t fileSize(const char *path) const;

        bool deleteFile(const char *path) const;

        bool exists(const char *path) const;

        void getFilesPath(const char *dirname, uint8_t levels, list<string> *paths);

        static string getFileExtension(const string &path);

        FS &fs = LittleFS;
    };
}

extern ls::LocalStorage localStorage;