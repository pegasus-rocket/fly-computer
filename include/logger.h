#pragma once
#include <USBCDC.h>

enum LogLevel {
    DEBUG,
    INFO,
    WARN,
    ERROR,
    HIDDEN // This state is used to hide all the log
};

namespace logger {
    class Logger {
    public:
        void setLogLevel(const LogLevel level) {
            logLevel = level;
        }

        void enableLevelName(const bool enabled) {
            logLevelEnabled = enabled;
        }


        /**
         * Logs a message with a specified log level, using a format string and optional arguments.
         *
         * This function logs a formatted message to the Serial output, based on the provided log level.
         * The log level determines the severity of the message, and the message itself is formatted
         * using a format string and a variable number of arguments, just like `printf`.
         * The message will only be logged if the specified log level is greater than or equal to the
         * current set `logLevel`. Additionally, the message is logged with an optional prefix based on
         * the log level (e.g., "[DEBUG]" or "[ERROR]") if `logLevelEnabled` is set to true.
         *
         * @param level The log level, which determines the severity of the message.
         *              Valid values are:
         *              - `DEBUG`: Used for debug-level messages, typically used during development.
         *              - `INFO`: General informational messages.
         *              - `WARN`: Warning messages that indicate potential issues.
         *              - `ERROR`: Error messages that indicate serious issues that need attention.
         *              - `HIDDEN`: Messages with this level are not logged (hidden).
         *
         * @param format A format string that defines how the arguments will be formatted. It follows
         *               the same format as `printf`, and can include placeholders (e.g., `%d`, `%s`).
         *
         * @param args A variable argument list that corresponds to the placeholders in the format string.
         *             It should be provided by calling `va_start()` in the calling function. The number and
         *             types of arguments should match the format string placeholders.
         *
         * @note If the formatted message exceeds the buffer size, it will be truncated and null-terminated.
         *       If `logLevelEnabled` is true, a log level prefix will be added to the message, otherwise,
         *       only the formatted message is logged.
         *
         * @example
         * // Example usage:
         * // To log an informational message:
         * va_list args;
         * va_start(args, format);
         * log(INFO, "Value of x is: %d", args);
         * va_end(args);
         */
        void log(LogLevel level, const char *format, va_list args) const;

        /**
         * Logs a message with a specified log level, using a format string and optional arguments.
         *
         * This function logs a formatted message to the Serial output based on the provided log level.
         * The log level determines the severity of the message, and the message is formatted using a
         * format string and a variable number of arguments, just like `printf`.
         * The message will only be logged if the specified log level is greater than or equal to the
         * currently set `logLevel`. Additionally, the message is logged with an optional prefix based on
         * the log level (e.g., "[DEBUG]" or "[ERROR]") if `logLevelEnabled` is set to true.
         *
         * @param level The log level, which determines the severity of the message.
         *              Valid values are:
         *              - `DEBUG`: Used for debug-level messages, typically used during development.
         *              - `INFO`: General informational messages.
         *              - `WARN`: Warning messages that indicate potential issues.
         *              - `ERROR`: Error messages that indicate serious issues that need attention.
         *              - `HIDDEN`: Messages with this level are not logged (hidden).
         *
         * @param format A format string that defines how the arguments will be formatted. It follows
         *               the same format as `printf`, and can include placeholders (e.g., `%d`, `%s`).
         *
         * @param ... A variable number of arguments that correspond to the placeholders in the format string.
         *            These arguments should match the number and types of placeholders in the format string.
         *
         * @note If the formatted message exceeds the buffer size, it will be truncated and null-terminated.
         *       If `logLevelEnabled` is true, a log level prefix will be added to the message, otherwise,
         *       only the formatted message is logged.
         *
         * @example
         * // Example usage:
         * // To log an informational message:
         * log(INFO, "Value of x is: %d", x);
         *
         * // To log a debug message:
         * log(DEBUG, "Debugging function %s at line %d", __FUNCTION__, __LINE__);
         */
        void log(LogLevel level, const char *format, ...) const;

        /**
         * Logs a debug message using a format string and optional arguments.
         *
         * This function logs a message with the `DEBUG` log level, formatted according to the provided
         * format string and optional arguments, similar to `printf`. It allows logging debug-specific
         * messages that are typically useful during development.
         *
         * @param format A format string that defines how the arguments will be formatted. It follows
         *               the same format as `printf`, and can include placeholders (e.g., `%d`, `%s`).
         *
         * @param ... A variable number of arguments that correspond to the placeholders in the format string.
         *            These arguments should match the number and types of placeholders in the format string.
         *
         * @example
         * // Example usage:
         * int value = 42;
         * debug("Debugging value: %d", value);
         */
        void debug(const char *format, ...) const;

        /**
         * Logs an informational message using a format string and optional arguments.
         *
         * This function logs a message with the `INFO` log level, formatted according to the provided
         * format string and optional arguments, similar to `printf`. It allows logging general information
         * about the system or application state.
         *
         * @param format A format string that defines how the arguments will be formatted. It follows
         *               the same format as `printf`, and can include placeholders (e.g., `%d`, `%s`).
         *
         * @param ... A variable number of arguments that correspond to the placeholders in the format string.
         *            These arguments should match the number and types of placeholders in the format string.
         *
         * @example
         * // Example usage:
         * info("System initialized with version %s", version);
         */
        void info(const char *format, ...) const;

        /**
         * Logs a warning message using a format string and optional arguments.
         *
         * This function logs a message with the `WARN` log level, formatted according to the provided
         * format string and optional arguments, similar to `printf`. It allows logging messages that warn
         * of potential issues or situations that may require attention.
         *
         * @param format A format string that defines how the arguments will be formatted. It follows
         *               the same format as `printf`, and can include placeholders (e.g., `%d`, `%s`).
         *
         * @param ... A variable number of arguments that correspond to the placeholders in the format string.
         *            These arguments should match the number and types of placeholders in the format string.
         *
         * @example
         * // Example usage:
         * warning("Low memory warning. Available memory: %d", availableMemory);
         */
        void warning(const char *format, ...) const;

        /**
         * Logs an error message using a format string and optional arguments.
         *
         * This function logs a message with the `ERROR` log level, formatted according to the provided
         * format string and optional arguments, similar to `printf`. It allows logging messages that indicate
         * errors or serious issues that need immediate attention.
         *
         * @param format A format string that defines how the arguments will be formatted. It follows
         *               the same format as `printf`, and can include placeholders (e.g., `%d`, `%s`).
         *
         * @param ... A variable number of arguments that correspond to the placeholders in the format string.
         *            These arguments should match the number and types of placeholders in the format string.
         *
         * @example
         * // Example usage:
         * error("Failed to open file: %s", filename);
         */
        void error(const char *format, ...) const;

        /**
         * Converts a log level to its corresponding string representation.
         *
         * This static function converts a `LogLevel` enum value to a string representation for
         * easier logging and output. The string value returned corresponds to the name of the log level
         * (e.g., "DEBUG", "INFO", "WARN", "ERROR", or "HIDDEN").
         *
         * @param level The log level to convert. Must be one of the `LogLevel` values, such as
         *              `DEBUG`, `INFO`, `WARN`, `ERROR`, or `HIDDEN`.
         *
         * @return A string representation of the provided log level.
         *
         * @example
         * // Example usage:
         * LogLevel level = DEBUG;
         * const char *levelStr = logLevelToString(level);  // Returns "DEBUG"
         */
        static const char *logLevelToString(LogLevel level);

    private:
        LogLevel logLevel = DEBUG;
        bool logLevelEnabled = true;
    };
}

extern logger::Logger Sl;
