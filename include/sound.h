#pragma once
#include "settings.h"
#define BUZZER_PIN 8

// Please always put NO_SOUND at the end.
enum sounds { ERROR_SOUND, READY_SOUND, FLYING_SOUND, NO_SOUND };

typedef struct Sound_s {
    int *notes;      // Array of notes
    int *durations;  // Array of duration for each note
    int numberOfNotes;
} Sound;

/**
 * Play a selected sound on a buzzer.
 * This function plays predefined melodies based on the selected sound type.
 * It uses a buzzer connected to the specified pin to produce sound.
 * The melodies are stored as arrays of notes and durations.
 *
 * @note Make sure to define the note and duration arrays for each sound type
 * (READY_SOUND, ERROR_SOUND, LANDING_SOUND) before calling this function.
 */
void playSound();

/**
 * Change the selected sound for playback.
 * This function allows you to select a sound type for playback, such as
 * READY_SOUND, ERROR_SOUND, LANDING_SOUND, or NO_SOUND (to mute the buzzer).
 *
 * @param sound An integer representing the desired sound type.
 *              - ERROR_SOUND: Selects an error sound.
 *              - READY_SOUND: Selects a ready sound.
 *              - LANDING_SOUND: Selects a landing sound.
 *              - NO_SOUND: Turns off the buzzer sound (no sound).
 *              Any other value will be ignored.
 */
void changeSound(int sound);

/**
 * Call the function tone with a given frequency and duration.
 * @param frequency
 * @param duration
 */
void playTone(unsigned int frequency, unsigned long duration);

/**
 * Stop the tone.
 */
void stopTone();