#pragma once
#define STORAGE_ALTITUDE 0x0

/**
 * @class Storage
 * @brief A class for storing and retrieving items.
 */

class Storage {
   public:
    /**
     * @brief Store an integer value in EEPROM at a specified memory address.
     *
     * This function is a part of the Storage class and allows you to store an
     integer value in the EEPROM (Electrically Erasable Programmable Read-Only
     Memory) of an Arduino or compatible microcontroller. The EEPROM is
     non-volatile memory that retains data even when the device is powered off.

     * @param key The memory address where the integer value should be stored.
     This address should be within the valid range of the EEPROM on your
     microcontroller.
     * @param value The integer value to be stored in EEPROM. This value will be
     associated with the specified memory address.
     *
     * @note Be cautious when choosing the memory address (key), as overwriting
     critical data stored in EEPROM can have unintended consequences.
     *
     * @see https://www.arduino.cc/en/Reference/EEPROM for more information
     about using EEPROM in Arduino.
     */
    static void addItem(int key, int value);

    /**
     * @brief Clears the storage by removing all stored items.
     *
     * This function deletes all items stored in the storage.
     * After calling this function, the storage will become empty.
     * Use this function with caution as it permanently deletes all data.
     *
     * @note This function does not affect the storage itself.
     *       It only removes the items stored in the storage.
     */
    static void clearStorage();

    /**
     * Retrieves an integer value from EEPROM storage at the specified address.
     *
     * This function reads an integer value stored in the EEPROM memory at the
     * given address (key) and updates the provided integer pointer (value) with
     * the retrieved value. The EEPROM is a non-volatile memory that can store
     * data even when the Arduino is powered off.
     *
     * @param key The address (location) in EEPROM from which to read the value.
     * @param value A pointer to an integer where the retrieved value will be
     * stored.
     *
     * @note Ensure that the `value` pointer points to a valid memory location.
     * @note The EEPROM stores values as bytes, so this function retrieves an
     * integer by reading the corresponding bytes from EEPROM.
     *
     * @see Storage::setItem(int key, int value)
     */
    static void getItem(int key, int* value);

    /**
     * @brief Prints the contents of EEPROM storage.
     *
     * This function iterates through the EEPROM and prints the stored data.
     *
     * @note The function does not take any parameters and does not return any
     * values. It is used for debugging or monitoring the content of the EEPROM
     * memory.
     *
     * @warning This function does not perform any error checking and assumes
     * that the EEPROM library is initialized properly before use.
     */
    static void printStorage();

    /**
     * @brief Prints the value associated with a given key from EEPROM.
     *
     * This function reads a byte value from EEPROM storage at the specified key
     * and prints the key along with its associated value to the Serial monitor.
     *
     * @param key The key used to identify the location in EEPROM from which to
     * read.
     */
    static void printItem(int key);

    /**
     * Update an item in EEPROM storage.
     *
     * This function allows you to update a value associated with a specific key
     * in the EEPROM memory of your Arduino. The EEPROM is a form of
     * non-volatile memory that retains data even when the power is removed. You
     * can use this function to store and update configuration parameters,
     * settings, or any other data that should persist across power cycles.
     *
     * @param key The address or location in EEPROM where the data is stored.
     * @param value The new value to be stored at the specified key.
     *
     * @note The EEPROM memory is limited in size, and it has a finite number of
     * write cycles. Excessive writes may wear out the EEPROM over time, so use
     * this function judiciously for data that needs to be updated infrequently.
     */
    static void updateItem(int key, int value);
};
