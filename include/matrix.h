#pragma once
#include "vector.h"

//-------------------------------------------------------------------------------------------------
// Structure Mat3

typedef struct Mat3_s {
    float data[3][3];
} Mat3;

/// Return the determinant of 3x3 matrix.
/// \param mat 3x3 matrix
/// \return The determinant
float Mat3_Det(const Mat3 &mat);

/// Multiply the coefficients of a 4x4 matrix by a scalar
/// \param matrix The matrix to scale.
/// \param s The scalar.
/// \return scaled matrix.
Mat3 Mat3_Sale(Mat3 matrix, float s);

/// Multiply a 3x3 matrix with a vector of size 3.
/// \param mat The matrix
/// \param v The vector
/// \return Return a vector of size 3
Vec3 *Mat3_MulMV(const Mat3 &mat, const Vec3 *v);

Mat3 Mat3_MulMM(const Mat3 &mat1, const Mat3 &mat2);

/// \brief Print matrix in tty console via serial.
/// \param matrix The matrix to print.
void Mat3_printMatrix(Mat3 matrix);

Mat3 Mat3_rotationX(float angle);

Mat3 Mat3_rotationY(float angle);

Mat3 Mat3_rotationZ(float angle);

//-------------------------------------------------------------------------------------------------
// Structure Vec4

/// \brief Cartesian matrix
typedef struct Mat4_s {
    float data[4][4];
} Mat4;

/// \brief Convert euler vector to a quaternion matrix.
/// \param v The vector to convert
/// \return The quaternion matrix
Mat4 Mat4_eulerToQuat(const Vec3* v);

/// \brief Multiply a 4x4 matrix with a vector of size 4.
/// \param mat The matrix
/// \param v The vector
/// \return Return a vector of size 4;
Vec4 *Mat4_MulMV(const Mat4 &mat, const Vec4 *v);

/// Transform a quaternion vector to a matrix.
/// \param vector The vector
/// \return Mat4 matrix quaternion
Mat4 Mat4_setQua(const Vec4 *vector);

/// Cofactor of a matrix with the row i and col j.
/// This corresponds to (-1)^(i+j) * det(M_ij)
/// \param mat The matrix
/// \param i row index
/// \param j col index
/// \return The cofactor of the matrix.
float Mat4_Cofactor(const Mat4 &mat, int i, int j);

/// \brief Print matrix in tty console via serial.
/// \param matrix The matrix to print.
void Mat4_printMatrix(Mat4 matrix);