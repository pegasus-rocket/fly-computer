#pragma once
#include "ESPAsyncWebServer.h"
#include "SPIFFS.h"
#include <list>

using namespace std;

#define PORT 80
#define FILES_LEVELS 3

typedef struct ServerRules_s {
    String path;
    bool isDirectory;
    bool allowDownload;
    const char *overwriteUrl;
    const char *redirectUrl;
} ServerRules;

class WebServer {
public:
    WebServer() : server(PORT) {};

    void add(
            String path,
            bool isDirectory,
            bool allowDownload,
            const char *overwriteUrl = nullptr,
            const char *redirectUrl = nullptr);

    void begin();

private:
    AsyncWebServer server;
    list<ServerRules> rules;

    static String getFileType(const String &extension);
};